//
//  ViewController.swift
//  Hello Single-View
//
//  Created by Konstantin Ay on 2017-10-14.
//  Copyright © 2017 KonstantinAy. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    var audioPlayer: AVAudioPlayer = AVAudioPlayer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let yeahFile = Bundle.main.path(forResource: "yeah", ofType: "mp3")
        
        do{
            try audioPlayer = AVAudioPlayer(contentsOf: URL(fileURLWithPath: yeahFile!))
        }catch{
            print(error)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: Actions
    @IBAction func buttonTap(_ sender: Any) {
        audioPlayer.play()
    }
    
}

